package main

/* zfec.go
 *
 * Copyright (C) 2013 Simon Liu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

import (
	"fmt"
	"gitlab.com/zfec/go-zfec.git"
	flag "launchpad.net/gnuflag"
	"os"

	"runtime"
)

var outputDir string
var prefix string
var suffix string
var totalShares uint
var requiredShares uint
var force bool
var verbose bool
var quiet bool
var version bool
var help bool
var numCpus int

const (
	versionString = "1.0"
)

func PrintUsageHeadline() {
	fmt.Fprintf(os.Stderr, "%s: [-h] [-c C] [-d D] [-p P] [-s S] [-m M] [-k K] [-f] [-v] [-q] [-V] INF\n", os.Args[0])
}

func PrintUsageSummary() {
	fmt.Fprintf(os.Stderr, `Encode a file into a set of share files, a subset of which can later be used to recover the original file.

positional arguments:
  INF, file to encode or "-" for stdin

optional arguments:
`)
}

func init() {
	var CustomUsage = func() {
		PrintUsageHeadline()
	}

	const (
		defaultOutputDir      = "."
		defaultTotalShares    = 8
		defaultRequiredShares = 3
		defaultSuffix         = "fec"
		outputDirUsage        = "directory in which share file names will be created"
		prefixUsage           = "prefix for share file names; If omitted, the name of the input file will be used"
		suffixUsage           = "suffix for share file names"
		totalSharesUsage      = "the total number of share files created"
		requiredSharesUsage   = "the number of share files required to reconstruct"
		forceUsage            = "overwrite any file which already in place an output file (share file)"
		verboseUsage          = "print out messages about progress"
		quietUsage            = "quiet progress indications and warnings about silly choices of K and M"
		versionUsage          = "print out version number and exit"
		helpUsage             = "show this help message and exit"
		numCpusUsage          = "Use n processors (default is the total number of processor cores)"
	)
	defaultNumCpus := runtime.NumCPU()
	flag.StringVar(&outputDir, "output-dir", defaultOutputDir, outputDirUsage)
	flag.StringVar(&outputDir, "d", defaultOutputDir, outputDirUsage)
	flag.StringVar(&prefix, "p", "", prefixUsage)
	flag.StringVar(&prefix, "prefix", "", prefixUsage)
	flag.StringVar(&suffix, "s", defaultSuffix, suffixUsage)
	flag.StringVar(&suffix, "suffix", defaultSuffix, suffixUsage)
	flag.UintVar(&totalShares, "m", defaultTotalShares, totalSharesUsage)
	flag.UintVar(&totalShares, "totalshares", defaultTotalShares, totalSharesUsage)
	flag.UintVar(&requiredShares, "k", defaultRequiredShares, requiredSharesUsage)
	flag.UintVar(&requiredShares, "requiredshares", defaultRequiredShares, requiredSharesUsage)
	flag.IntVar(&numCpus, "c", defaultNumCpus, numCpusUsage)
	flag.IntVar(&numCpus, "numcpus", defaultNumCpus, numCpusUsage)
	flag.BoolVar(&force, "f", false, forceUsage)
	flag.BoolVar(&force, "force", false, forceUsage)
	flag.BoolVar(&verbose, "v", false, verboseUsage)
	flag.BoolVar(&verbose, "verbose", false, verboseUsage)
	flag.BoolVar(&quiet, "q", false, quietUsage)
	flag.BoolVar(&quiet, "quiet", false, quietUsage)
	flag.BoolVar(&version, "V", false, versionUsage)
	flag.BoolVar(&version, "version", false, versionUsage)
	flag.BoolVar(&help, "h", false, helpUsage)
	flag.BoolVar(&help, "help", false, helpUsage)
	flag.Usage = CustomUsage
}

func main() {
	flag.Parse(true)

	if help || (flag.NArg() == 0 && flag.NFlag() == 0) {
		PrintUsageHeadline()
		fmt.Fprintln(os.Stderr, "")
		PrintUsageSummary()
		flag.PrintDefaults()
		return
	}

	if version {
		fmt.Printf("zfec library version: %s\n", zfec.VersionString)
		fmt.Printf("zfec (go) command-line tool version: %s\n", versionString)
		return
	}

	if flag.NArg() != 1 {
		fmt.Println("Missing file argument INF")
		PrintUsageHeadline()
		return
	}

	if quiet && verbose {
		fmt.Println("Please choose only one of --verbose and --quiet.")
		PrintUsageHeadline()
		return
	}

	dirinfo, err := os.Stat(outputDir)
	if err != nil {
		fmt.Println("Output directory does not exist.")
		return
	}

	if !dirinfo.IsDir() {
		fmt.Println("Output directory is not a valid directory.")
		return
	}

	if totalShares > 256 || totalShares < 1 {
		fmt.Println("Invalid parameters, totalshares is required to be <= 256 and >= 1\nPlease see the accompanying documentation.")
		return
	}

	if requiredShares > totalShares || requiredShares < 1 {
		fmt.Println("Invalid parameters, requiredshares is required to be <= totalshares and >= 1\nPlease see the accompanying documentation.")
		return
	}

	if numCpus > runtime.NumCPU() || numCpus < 1 {
		fmt.Println("Invalid parameters, number of cpus is required to be at least 1 and less than the total number of processor cores.")
		return
	}
	runtime.GOMAXPROCS(numCpus)

	z, err := zfec.Init(int(requiredShares), int(totalShares))
	if err != nil {
		fmt.Println(err)
		return
	}

	_, err = z.EncodeToFiles(flag.Arg(0), outputDir, "", prefix, suffix, force, verbose, numCpus)
	if err != nil {
		fmt.Println(err)
		return
	}

}
