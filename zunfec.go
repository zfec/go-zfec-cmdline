package main

/* zunfec.go
 *
 * Copyright (C) 2013 Simon Liu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

import (
	"fmt"
	flag "launchpad.net/gnuflag"
	"os"
	"gitlab.com/zfec/go-zfec.git"
)

const (
	versionString = "1.0"
)

var outputFile string
var force bool
var verbose bool
var version bool
var help bool

func PrintUsageHeadline() {
	fmt.Fprintf(os.Stderr, "%s: [-h] -o OUTF [-v] [-f] [-V] [SHAREFILE [SHAREFILE ...]]\n", os.Args[0])
}

func PrintUsageSummary() {
	fmt.Fprintf(os.Stderr, `Decode data from share files.

positional arguments:
  SHAREFILE             shares file to read the encoded data from
	
optional arguments:
`)
}

func init() {
	var CustomUsage = func() {
		PrintUsageHeadline()
	}

	const (
		defaultOutputFile = ""
		outputFileUsage   = "file to write the resulting data to, or \"-\" for stdout"
		forceUsage        = "overwrite any file which is already in place of the output file"
		verboseUsage      = "print out messages about progress"
		versionUsage      = "print out version number and exit"
		helpUsage         = "show this help message and exit"
	)
	// FIXME: TEST STDOUT AND STDIN
	flag.StringVar(&outputFile, "output-file", defaultOutputFile, outputFileUsage)
	flag.StringVar(&outputFile, "o", defaultOutputFile, outputFileUsage)
	flag.BoolVar(&force, "f", false, forceUsage)
	flag.BoolVar(&force, "force", false, forceUsage)
	flag.BoolVar(&verbose, "v", false, verboseUsage)
	flag.BoolVar(&verbose, "verbose", false, verboseUsage)
	flag.BoolVar(&version, "V", false, versionUsage)
	flag.BoolVar(&version, "version", false, versionUsage)
	flag.BoolVar(&help, "h", false, helpUsage)
	flag.BoolVar(&help, "help", false, helpUsage)
	flag.Usage = CustomUsage
}

func main() {

	flag.Parse(true)

	if help || (flag.NArg() == 0 && flag.NFlag() == 0) {
		PrintUsageHeadline()
		fmt.Fprintln(os.Stderr, "")
		PrintUsageSummary()
		flag.PrintDefaults()
		return
	}

	if version {
		fmt.Printf("zfec library version: %s\n", zfec.VersionString)
		fmt.Printf("zunfec (go) command-line tool version: %s\n", versionString)
		return
	}

	if outputFile == "" {
		fmt.Println("error: argument -o/--outputfile is required")
		PrintUsageHeadline()
		return
	}

	if flag.NArg() < 2 {
		fmt.Println("At least two sharefiles are required.")
		PrintUsageHeadline()
		return
	}

	err := zfec.DecodeToFile(outputFile, flag.Args(), force, verbose)
	if err != nil {
		fmt.Println(err)
		return
	}
}
